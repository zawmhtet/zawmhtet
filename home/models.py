from django.db import models

# Create your models here.

class blog(models.Model):
    title = models.CharField(max_length='100')
    post = models.TextField(blank = False)
    date = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return self.title
