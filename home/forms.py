from django import forms

class artSciForm(forms.Form):
    textField = forms.CharField(widget=forms.Textarea)

class imgForm(forms.Form):
    imageField = forms.ImageField()
