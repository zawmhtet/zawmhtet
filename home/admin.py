from django.contrib import admin
from home.models import *

class blogAdmin(admin.ModelAdmin):
	fields = ['post','title']

admin.site.register(blog)
