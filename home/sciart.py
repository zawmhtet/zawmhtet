import Image
import math

def magic(usrInput):
    ##usrInput = raw_input("Text filename: ")
    ##txtfile = open(usrInput,"r")
##    li = txtfile.readlines()
    wordcount = 0
    letters = []
##    for i in range(len(li)):
##        letters += li[i]
    letters += usrInput
##    print letters
    wordcount = len(letters)
##    print "wordcount", wordcount
    numpixels = getnumpixels(wordcount)
##    print "num pixels" , numpixels
    x, y = getdimension(numpixels)
##    print "dimension" , (x,y), "total: ", (x*y)
    xtrapixels = (x*y)-numpixels
##    print "xtrapixels", xtrapixels
    count =0
    im = Image.new("RGB",(x,y))
    for j in range(y):
        for i in range(x):
            if len(letters) >= 3:
                b = ord(letters[2])
                letters.pop(2)
                g = ord(letters[1])
                letters.pop(1)
                r = ord(letters[0])
                letters.pop(0)
                im.putpixel((i,j),(r,g,b))
                count +=1
            else:
                if xtrapixels != 0:
                    im.putpixel((i,j),(0,0,0))
                    xtrapixels -= 1
                    count += 1
##    im.show()
    im.save("templates/static/artsciimg.png")
##    print count
##    print letters

def reversemagic(usrInput):
    im = Image.open(usrInput).convert("RGB")
    w, h = im.size
    bigstring = ""
    for j in range(h):
        for i in range(w):
            r,g,b = im.getpixel((i,j))
            letter1 = chr(r)
            letter2 = chr(g)
            letter3 = chr(b)
            bigstring += letter1 + letter2 + letter3
    f = file("templates/static/imgtoText.txt","w")
    f.write(bigstring)
    f.close()
    return len(bigstring)


def getnumpixels(wordcount):
    return (wordcount/3)

def primefinder(N):
    prime=True
    for divisor in range(2,N):
        if N%divisor==0:
            prime=False
            break
    return prime

def getdimension(num):
    c = math.sqrt(num)
    ci = int(c)
    if c == int(c):
        c = int(c)
        return (c,c)
    else:
        if (ci*(ci+1)) < num:
            return (ci,ci+2)
        return (ci,ci+1)
