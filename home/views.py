# Create your views here.
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render_to_response,render
from home.models import *
from home.forms import *
from home.sciart import magic,reversemagic

def home(request):
	return render_to_response('index.html',{})

def resume(request):
	return render_to_response('resume.html',{})

def contact(request):
	return render_to_response('contact.html',{})

def projects(request):
	return render_to_response('base.html',{})

def newindex(request):
	return render_to_response('newindex.html',{})

def debra(request):
	return render_to_response('debra.html',{})

def blogpost(request):
        posts = blog.objects.all()
        return render_to_response('blog.html',{'posts':posts})

def craftygame(request):
	return render_to_response('crafty.html',{})

def artSci(request):
        if request.method == 'POST':
                form = artSciForm(request.POST)
                imageform = imgForm(request.POST,request.FILES)
                if form.is_valid():
                        text = form.cleaned_data['textField']
                        magic(text)
                        lt = 2
                        return render_to_response('artsci2.html',{'lt':lt})
                elif imageform.is_valid():
                        img = imageform.cleaned_data['imageField']
                        bigtext = reversemagic(img)
                        return render_to_response('artsci2.html',{'bigtext':bigtext})
        else:
                form = artSciForm()
                imageform = imgForm()
        return render(request,'artsci.html',{'form':form,'img':imageform})
##                try:
##                        text = form.cleaned_data['textField']
##                except:
##                        pass
##                else:
##                        magic(text)
##                        lt = len(text)
##                        return render_to_response('artsci2.html',{"lt":lt})
##                
##                try:
##                        img = form.cleaned_data['imageField']
##                except:
##                        pass
##                else:
##                        bigtext = reversemagic(img)
##                        return render_to_response('artsci2.html',{'bigtext':bigtext})
##
##        else:
##                form = artSciForm()
##        return render(request,'artsci.html',{'form':form,})

def map(request):
        return render_to_response('map.html',{})
