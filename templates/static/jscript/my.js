var curPage="";
var defaultPage="#article_about";

$(document).ready(function(){
	$("#article_about").hide();
	$("#article_contact").hide();
	$("#article_resume").hide();
	$("#article_photography").hide();
	$("#article_projects").hide();
	
	var x = location.hash;
	if (x == ""){
		showPage(defaultPage);
	}
	else{ showPage(x);}

	$("#about").click(function(){
		showPage("#article_about");
	})
	
	$("#resume").click(function(){
		showPage("#article_resume");
	})

	$("#contact").click(function(){
		showPage("#article_contact");
	})

	$("#photography").click(function(){
		showPage("#article_photography");
	})

	$("#projects").click(function(){
		showPage("#article_projects");
	})
});

function showPage(selector){
	if (curPage!=""){
		$(curPage).animate({left:"50%"});
		$(curPage).hide(500);
		$(curPage).animate({left:"0"});
	}
	$(selector).delay(600);
	$(selector).show(1000);
	curPage = selector;
}