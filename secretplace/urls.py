from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'secretplace.views.home', name='home'),
    # url(r'^secretplace/', include('secretplace.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
        url(r'^admin/', include(admin.site.urls)),

	url(r'^$', 'home.views.home', name='home'),
	url(r'^resume', 'home.views.resume', name='resume'),
	url(r'^contactme','home.views.contact',name='contactme'),
	url(r'^projects','home.views.projects',name='projects'),
	url(r'^newindex','home.views.newindex',name='newindex'),
	url(r'^debra','home.views.debra',name='debra'),
        url(r'^blog','home.views.blogpost',name='blog'),
	url(r'^game','home.views.craftygame',name='game'),
        url(r'^artsci','home.views.artSci',name='artsci'),
        url(r'^map','home.views.map',name='map'),
)
